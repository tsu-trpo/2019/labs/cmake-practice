# cmake-practice

1. Создать папку `<my_second_name>` или `<our_second_names>` и перейти в неё.
2. Выполнить КР https://docs.google.com/presentation/d/1Me-WtSmGF7oUB1VDWg8mDyLuMHO10z2hwGCT_b_Z1Nw/edit?usp=sharing
3. Ответвиться от мастера, сделать коммит и залить ветку в `gitlab`.
4. Создать `Merge Request`, поле `assigne` должно указывать на @vosafonov, в описании нужно перечислить фамилии авторов работы.


Подсказка с тестами:
```c++
// main.cpp 
#include <gtest/gtest.h>

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

// rand.cpp
#include <gtest/gtest.h>
#include <rand.h>

TEST(Rand, Usage)
{
    Rand rand{10};
    int num = rand.rand();
    ASSERT_LE(num, 10);
    ASSERT_GE(num, 0);
}

```